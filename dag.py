#Default graph
data = {1 : [2,3,4,5],  
        2 : [6],
        3 : [6,7],
        4 : [7,8],
        5 : [8]
}
#Add a Node to the graph
def addNode(graph,node,relationship):
    graph[node]=relationship;
    return graph
    
#Remove node from the graph  
def removeNode(graph,node):
    del graph[node]
    graphCopy = graph.copy()
    #remove all occurance of node from relationship
    for keys in graphCopy:
        if(node in graph[keys]):
            graph[keys].remove(node)
            if(len(graph[keys])==0):
                removeNode(graphCopy.copy(),keys)
    return graph

#Update A node
def updateNode(graph,node,relationship):
    graph[node]=relationship;
   
    try:
        drawGraph(graph)
        return graph
    except:
        raise ValueError('Detected some issue with the graph')
        
#Print the graph   
def drawGraph(graph):
    return drawPaths(graph=graph,path=[list(graph.keys())[0]],paths=[]);
   
#print all Possible Paths 
def drawPaths(graph, path, paths = []):   
    lastElem = path[-1] 
    if lastElem in graph:
        for val in graph[lastElem]:
            new_path = path + [val]
            paths = drawPaths(graph, new_path, paths)
    else:
        paths = paths + [path]
    return paths

#As Required Implementation

print(drawPaths(graph = data.copy(), path = [1], paths = []))

#Other Implementation (UnComment to run)

# data = removeNode(data.copy(),3)
# print(drawPaths(graph = data.copy(), path = [1], paths = []))
# data = updateNode(data.copy(),1,[2,3,4,5])
# data = updateNode(data.copy(),3,[8])
# print(drawGraph(graph=data.copy()))



str1 = 'ankit'
str2 = 'kumar'
str3 = 'verma'

#Combitorial Function for String
def combination(a,b,c,index):
    if(index<len(a) and index<len(b) and index<len(c)):
        return a[index]+b[index]+c[index]
    else:
        raise ValueError('Length Mismatch') 
        
#Pattern generator
def pattern(a=str1,b=str2,c=str3,pat="",index=0):
    if(index==len(a)):
        return pat
    else:
        pat = pat + combination(a,b,c,index)
        return pattern(a,b,c,pat,index+1)
        
def printPattern():
    print(pattern())
    
printPattern()
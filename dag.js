let dag = (function () {
    addNodes = (graph, node, relationship) => {
        const copy = Object.assign({}, graph)
        copy[node] = relationship;
        return copy
    }
    updateNodes = (graph, node, relationship) => {
        const copy = Object.assign({}, graph)
        copy[node] = relationship;

        try {
            drawGraph(copy)
            return copy
        }
        catch (e) {
            throw new Error('Detected some issue with the graph')
        }
    }

    drawGraph = (graph) => {
        return drawPattern(graph = graph, path = [1], paths = []);
    }

    deleteNodes = (graph, node) => {
        const copy = Object.assign({}, graph)
        delete copy[node]
        //remove all occurance of node from relationship
        for (const [key, value] of Object.entries(copy)) {
            let index = copy[key].indexOf(node);
            if (index > 0) {
                copy[key].splice(index, 1);
            }
            if (copy[key].length == 0)
                deleteNodes(copy, key)
        }
        return copy
    }
    drawPattern = (graph, path, paths) => {
        const lastItem = path.length > 0 ? path[path.length - 1] : null;

        if (graph.hasOwnProperty(lastItem)) {
            graph[lastItem].map(function (item) {
                let newPath = path.concat();
                newPath.push(item)
                paths = drawPattern(graph, newPath, paths)
            })
        } else {
            paths.push(path)
        }
        return paths
    }

    return {
        drawPattern, deleteNodes, addNodes, updateNodes
    }
}());

const data = {
    1: [2, 3, 4, 5],
    2: [6],
    3: [6, 7, 11, 12],
    4: [7, 8],
    5: [8],
    7: [9, 10],

}

const run = () => {
    console.log('Available Paths', dag.drawPattern(data, [3], []))
    console.log('Delete', dag.deleteNodes(data, 3))
    console.log('Update', dag.updateNodes(data, 3, [7, 8]))
}
run()

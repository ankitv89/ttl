const str1 = 'ankit'
const str2 = 'kumar'
const str3 = 'verma'

const pattern = (function(){
 //Combitorial Function for String
    const combination = (a,b,c,index)=>{
        if(a.length==b.length && b.length==c.length && c.length==a.length && index<a.length){
            return a[index]+b[index]+c[index]
        }
        else
        throw new Error('Length Mismatch')
    }
    const createPattern = (a,b,c,pat,index)=>{
        if(!index) index = 0;
        if(!pat) pat = "";
        if(index==a.length){
            return pat
        }
        else {
            pat = pat + combination(a,b,c,index)
            return createPattern(a,b,c,pat,index+1)
        }
    }
    return {createPattern}
}());

        


console.log(pattern.createPattern(str1,str2,str3))
